import { z } from 'zod';

export const loginSchema = z.object({
  email: z.string().email({ message: 'You must enter valid email' }),
  password: z.string({ required_error: 'Password is required' }).min(1),
});

export type FormLoginSchema = z.infer<typeof loginSchema>;
