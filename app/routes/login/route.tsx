import type { ActionFunctionArgs, MetaFunction } from '@remix-run/node';
import { Form, json } from '@remix-run/react';
import { zodResolver } from '@hookform/resolvers/zod';
import { Button, Input } from '@nextui-org/react';
import { Controller } from 'react-hook-form';
import { getValidatedFormData, useRemixForm } from 'remix-hook-form';

import { FormLoginSchema, loginSchema } from '~/lib/schema/auth-schema';
import AuthLayout from './layout';

const resolver = zodResolver(loginSchema);

export const meta: MetaFunction = () => {
  return [
    { title: 'Login - Website name' },
    { name: 'description', content: 'Login to your web application account' },
  ];
};

export const action = async ({ request }: ActionFunctionArgs) => {
  const {
    data,
    errors,
    receivedValues: defaultValues,
  } = await getValidatedFormData<FormLoginSchema>(request, resolver);
  if (errors) {
    return json({ errors, defaultValues });
  }

  console.error(errors);
  console.log(data);

  return null;
};

export default function LoginPage() {
  const {
    handleSubmit,
    formState: { errors },
    control,
  } = useRemixForm<FormLoginSchema>({
    mode: 'onChange',
    resolver,
  });

  return (
    <AuthLayout>
      <div className="mt-16 w-full rounded bg-white p-10 shadow md:w-1/2 lg:w-1/4">
        <h1
          aria-label="Login to your account"
          className="pb-6 text-center text-xl font-semibold"
        >
          Login to your account
        </h1>
        <Form onSubmit={handleSubmit} className="flex flex-col gap-6">
          <Controller
            control={control}
            name="email"
            rules={{ required: true }}
            render={({ field }) => (
              <Input
                {...field}
                placeholder="Enter your email"
                label="Email"
                labelPlacement="outside"
                variant="bordered"
                type="email"
                value={field.value}
                errorMessage={errors.email?.message}
                isRequired
              />
            )}
          />
          <Controller
            control={control}
            name="password"
            render={({ field }) => (
              <Input
                {...field}
                placeholder="Enter your password"
                label="Password"
                labelPlacement="outside"
                variant="bordered"
                type="password"
                value={field.value}
                errorMessage={errors.password?.message}
                isRequired
              />
            )}
          />
          <div>
            <hr className="mb-6 border-foreground-200" />
            <Button type="submit" color="primary" fullWidth>
              Login account
            </Button>
          </div>
        </Form>
      </div>
    </AuthLayout>
  );
}
