import { Image } from '@nextui-org/react';

export default function AuthLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <div className="h-screen bg-gradient-to-tl from-blue-400 to-indigo-900 px-4 py-16">
      <div className="flex flex-col items-center justify-center">
        <Image
          src="https://tuk-cdn.s3.amazonaws.com/can-uploader/sign_in-svg1.svg"
          alt="logo"
        />
        {children}
      </div>
    </div>
  );
}
